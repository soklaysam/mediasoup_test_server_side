import { Router, Consumer, Producer, Transport } from "mediasoup/lib/types";

export interface Room {
  sockets: {
    [key:string]:string
  };

  router: Router;
  
  videoProducers: {
    [key:string]:Producer
  };
  
  audioProducers: {
    [key:string]:Producer
  };

  producerTransports: {
    [key:string]:Transport
  };

  consumerTransports: {
    [key:string]:Transport
  };

  videoConsumers: {
    [key:string]:{
      [remoteId:string]:Consumer
    }
  };

  audioConsumers: {
    [key:string]:{
      [remoteId:string]:Consumer
    }
  };
}