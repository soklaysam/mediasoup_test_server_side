import { ConnectedSocket, MessageBody, OnGatewayConnection, OnGatewayDisconnect, SubscribeMessage, WebSocketGateway} from "@nestjs/websockets";
import * as mediasoup from 'mediasoup';
import { Worker, RtpCodecCapability } from "mediasoup/lib/types";
import { mediasoupOptions } from "./config";
import { Room } from "./room.interface";

@WebSocketGateway(4000)
export class EventsGateway implements OnGatewayConnection, OnGatewayDisconnect {
  private worker: Worker = null;
  private rooms: {[key:string]:Room} = {};

  constructor(){
    this.init();
  }

  private async init(){
    try{
      this.worker = await mediasoup.createWorker();
      console.log('medisoup worker start');
    }
    catch(err){
      console.log('mediasoup error:', err);
    }
  }

  handleConnection(socket){
    console.log(socket.id, "connect");
  }

  handleDisconnect(socket){
    const id = socket.id;
    const room = socket.room;
    console.log(id, "disconnect");
    this.clearSocket(socket);
    if(!this.rooms[room].sockets){
      this.rooms[room].router.close();
      delete this.rooms[room];
    }
    socket.to(room).emit('userLeave', {remoteId: id, username: socket.username});
  }

  @SubscribeMessage('joinRoom')
  async joinRoom(@MessageBody() {room, username}, @ConnectedSocket() socket){
    const id = socket.id;
    if(!this.rooms[room]){
      const mediaCodecs = mediasoupOptions.router.mediaCodecs as RtpCodecCapability[];
      const router = await this.worker.createRouter({mediaCodecs});
      router.observer.on('close', ()=>{
        let transports = this.rooms[room].producerTransports;
        for(let key in transports) transports[key].close();
        transports = this.rooms[room].consumerTransports;
        for(let key in transports) transports[key].close();
      });
      this.rooms[room] = {
        sockets: {},
        router: router,
        producerTransports: {},
        consumerTransports: {},
        videoProducers: {},
        audioProducers: {},
        videoConsumers: {},
        audioConsumers: {}
      }
    }
    this.rooms[room].sockets[id]=username;
    socket.join(room);
    socket.room = room;
    socket.username = username;
    socket.to(room).emit('newUser', {remoteId: id, username});
    let users = [];
    for(let user in this.rooms[room].sockets){
      if(user != id) users.push({remoteId:user, username:this.rooms[room].sockets[user]});
    }
    return {users};
  }

  @SubscribeMessage('getRouterRtpCapabilities')
  getRouterRtpCapabilities(@ConnectedSocket() socket){
    const room = socket.room;
    return {rtpCapabilities: this.rooms[room].router.rtpCapabilities};
  }

  // Producer
  @SubscribeMessage('createProducerTransport')
  async createProducerTransport(@ConnectedSocket() socket){
    const id = socket.id;
    const room = socket.room;
    const { transport, params } = await this.createTransport(room);
    transport.observer.on('close', ()=>{
      const videoProducer = this.rooms[room].videoProducers[id];
      if (videoProducer) {
        videoProducer.close();
        delete this.rooms[room].videoProducers[id];
      }
      const audioProducer = this.rooms[room].audioProducers[id];
      if (audioProducer) {
        audioProducer.close();
        delete this.rooms[room].audioProducers[id];
      }
      delete this.rooms[room].producerTransports[id];
    });
    this.rooms[room].producerTransports[id] = transport;
    return {params};
  }
  
  @SubscribeMessage('connectProducerTransport')
  async connectProducerTransport(@MessageBody() {dtlsParameters}, @ConnectedSocket() socket){
    const id = socket.id;
    const room = socket.room;
    const transport = this.rooms[room].producerTransports[id];
    if (!transport) return {error: 'transport not exist'};
    await transport.connect({dtlsParameters});
    return {status: 'OK'};
  }

  @SubscribeMessage('produce')
  async produce(@MessageBody() {kind, rtpParameters}, @ConnectedSocket() socket){
    console.log('produce', kind);
    const id = socket.id;
    const room = socket.room;
    const transport = this.rooms[room].producerTransports[id];
    if (!transport) return {error: 'transport not exist'};
    const producer = await transport.produce({kind, rtpParameters});
    if(kind==='video') this.rooms[room].videoProducers[id] = producer;
    else if(kind==='audio') this.rooms[room].audioProducers[id] = producer;
    socket.to(room).emit('newProducer', {remoteId: id, kind});
    return {producerId: producer.id, kind: producer.kind};
  }

  @SubscribeMessage('closeProducer')
  closeProducer(@MessageBody() {producerId, kind}, @ConnectedSocket() socket){
    const id = socket.id;
    const room = socket.room;
    if(kind==='video'){
      const videoProducer = this.rooms[room].videoProducers[id];
      if (videoProducer) {
        videoProducer.close();
        delete this.rooms[room].videoProducers[id];
      }
    }
    else if(kind==='audio'){
      const audioProducer = this.rooms[room].audioProducers[id];
      if (audioProducer) {
        audioProducer.close();
        delete this.rooms[room].audioProducers[id];
      }
    }
    return {status:'ok'};
  }

  @SubscribeMessage('getProducers')
  getProducers(@ConnectedSocket() socket){
    const id = socket.id;
    const room = socket.room;
    let remoteVideoId = [];
    let remoteAudioId = [];
    for(let key in this.rooms[room].videoProducers){
      if(key !== id) remoteVideoId.push(key);
    }
    for(let key in this.rooms[room].audioProducers){
      if(key !== id) remoteAudioId.push(key);
    }
    return {remoteVideoId, remoteAudioId};
  }

  // Consumer
  @SubscribeMessage('createConsumerTransport')
  async createConsumerTransport(@ConnectedSocket() socket){
    const id = socket.id;
    const room = socket.room;
    const { transport, params } = await this.createTransport(room);
    transport.observer.on('close', ()=>{
      const videoConsumerSet = this.rooms[room].videoConsumers[id];
      if (videoConsumerSet) {
        for(let key in videoConsumerSet){
          const consumer = videoConsumerSet[key];
          consumer.close();
        }
        delete this.rooms[room].videoConsumers[id];
      }
      const audioConsumerSet = this.rooms[room].audioConsumers[id];
      if (audioConsumerSet) {
        for(let key in audioConsumerSet){
          const consumer = audioConsumerSet[key];
          consumer.close();
        }
        delete this.rooms[room].audioConsumers[id];
      }
      delete this.rooms[room].consumerTransports[id];
    });
    this.rooms[room].consumerTransports[id] = transport;
    return {params};
  }
  
  @SubscribeMessage('connectConsumerTransport')
  async connectConsumerTransport(@MessageBody() {dtlsParameters}, @ConnectedSocket() socket){
    const id = socket.id;
    const room = socket.room;
    const transport = this.rooms[room].consumerTransports[id];
    if (!transport) return {error: 'transport not exist'};
    await transport.connect({dtlsParameters});
    return {status: 'OK'};
  }

  @SubscribeMessage('consume')
  async consume(@MessageBody() {remoteId, kind, rtpCapabilities}, @ConnectedSocket() socket){
    const id = socket.id;
    const room = socket.room;
    const transport = this.rooms[room].consumerTransports[id];
    if (!transport) return {error: 'transport not exist'};
    let producer;
    if (kind==='video') producer = this.rooms[room].videoProducers[remoteId];
    else if (kind==='audio') producer = this.rooms[room].audioProducers[remoteId];
    if(!producer) return {error: 'producer not exist'};
    const producerId = producer.id;
    const consumer = await transport.consume({producerId, rtpCapabilities, paused: true}); //kind==='video'});
    consumer.observer.on('producersClose', ()=>{
      consumer.close();
      if(kind==='video') delete this.rooms[room].videoConsumers[id][remoteId];
      else if(kind==='audio') delete this.rooms[room].audioConsumers[id][remoteId];
    })
    if(!this.rooms[room].videoConsumers[id]) this.rooms[room].videoConsumers[id] = {};
    if(!this.rooms[room].audioConsumers[id]) this.rooms[room].audioConsumers[id] = {};
    if(kind==='video') this.rooms[room].videoConsumers[id][remoteId] = consumer;
    else if(kind==='audio') this.rooms[room].audioConsumers[id][remoteId] = consumer;
    return {
      id:consumer.id, 
      producerId, 
      kind: consumer.kind, 
      rtpParameters: consumer.rtpParameters,};
  }

  @SubscribeMessage('resume')
  async resume(@MessageBody() {remoteId, kind}, @ConnectedSocket() socket){
    const id = socket.id;
    const room = socket.room;
    let consumer;
    if(kind==='video') consumer = this.rooms[room].videoConsumers[id][remoteId];
    else if(kind==='audio') consumer = this.rooms[room].audioConsumers[id][remoteId];
    if (!consumer) return {error: 'consumer not exist'};
    await consumer.resume();
    return {status: 'OK'};
  }

  private clearSocket(socket: any) {
    const id = socket.id;
    const room = socket.room;
    let transport = this.rooms[room].producerTransports[id];
    if (transport) transport.close();
    transport = this.rooms[room].consumerTransports[id];
    if (transport) transport.close();
    delete this.rooms[room].sockets[id];
  }

  private async createTransport(room){
    const transport = await this.rooms[room].router.createWebRtcTransport(mediasoupOptions.webRtcTransport);
    return {
      transport: transport,
      params: {
        id: transport.id,
        iceParameters: transport.iceParameters,
        iceCandidates: transport.iceCandidates,
        dtlsParameters: transport.dtlsParameters
      }
    }
  }
}